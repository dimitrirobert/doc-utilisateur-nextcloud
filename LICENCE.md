# Licence publique Creative Commons Attribution - Partage dans les mêmes conditions 4.0 International

Lorsque Vous exercez les Droits accordés par la licence (définis
ci-dessous), Vous acceptez d\'être lié par les termes et conditions de
la présente Licence publique Creative Commons Attribution - Partage dans
les mêmes conditions 4.0 International (la « Licence publique »). Dans
la mesure où la présente Licence publique peut être interprétée comme un
contrat, Vous bénéficiez des Droits accordés par la licence en
contrepartie de Votre acceptation des présents termes et conditions, et
le Donneur de licence Vous accorde ces droits en contrepartie des
avantages que lui procure le fait de mettre à disposition l'Œuvre sous
licence en vertu des présents termes et conditions.

**Article 1 - Définitions.**

a.  [**Œuvre dérivée** signifie œuvre protégée par les Droit d'auteur et
    droits connexes, dérivée ou adaptée de l'Œuvre sous licence et dans
    laquelle l'Œuvre sous licence est traduite, retouchée, arrangée,
    transformée, ou modifiée de telle façon que l'autorisation du
    Donneur de licence est nécessaire, conformément aux dispositions des
    Droit d'auteur et droits connexes. Dans le cas de la présente
    Licence publique, lorsque l'Œuvre sous licence est une œuvre
    musicale, une représentation publique ou un enregistrement sonore,
    la synchronisation de l\'Œuvre sous licence avec une image animée
    sera considérée comme une Œuvre dérivée aux fins de la présente
    Licence publique.]{#s1a}
b.  [**Licence d'Œuvre dérivée** signifie licence par laquelle Vous
    accordez Vos Droit d\'auteur et droits connexes portant sur Vos
    contributions à l\'Œuvre dérivée, selon les termes et conditions de
    la présente Licence publique.]{#s1b}
c.  [**Licence compatible BY-SA** signifie licence figurant à l\'adresse
    suivante
    [creativecommons.org/compatiblelicenses](//creativecommons.org/compatiblelicenses),
    approuvée par Creative Commons comme étant essentiellement
    équivalente à la présente Licence publique.]{#s1c}
d.  [**Droit d'auteur et droits connexes** signifie droit d'auteur et/ou
    droits connexes incluant, notamment, la représentation, la radio et
    télédiffusion, l'enregistrement sonore et le Droit sui generis des
    producteurs de bases de données, quelle que soit la classification
    ou qualification juridique de ces droits. Dans le cadre de la
    présente Licence publique, les droits visés à l'Article
    [2(b)(1)-(2)](#s2b) ne relèvent ni du Droit d'auteur ni de droits
    connexes.]{#s1d}
e.  [**Mesures techniques efficaces** signifie mesures techniques qui,
    en l'absence d'autorisation expresse, ne peuvent être contournées
    dans le cadre de lois conformes aux dispositions de l'Article 11 du
    Traité de l'OMPI sur le droit d'auteur adopté le 20 Décembre 1996
    et/ou d'accords internationaux de même objet.]{#s1e}
f.  [**Exceptions et limitations** signifie utilisation loyale et
    équitable (*fair use* et *fair dealing*) et/ou toute autre exception
    ou limitation applicable à Votre utilisation de l'Œuvre sous
    licence.]{#s1f}
g.  [**Eléments de licence** signifie les composantes de la licence
    figurant dans l'intitulé de la Licence publique Creative Commons.
    Les éléments de la présente Licence publique sont : Attribution et
    Partage dans les mêmes conditions.]{#s1g}
h.  [**Œuvre sous licence** signifie œuvre littéraire ou artistique,
    base de données ou toute autre œuvre pour laquelle le Donneur de
    licence a recours à la présente Licence publique.]{#s1h}
i.  [**Droits accordés par la licence** signifie droits qui Vous sont
    accordés selon les termes et conditions d'utilisation définis par la
    présente Licence publique, limités aux Droit d'auteur et droits
    connexes applicables à Votre utilisation de l'Œuvre sous licence et
    que le Donneur de licence a le droit d'accorder.]{#s1i}
j.  [**Donneur de licence** signifie un individu ou une entité octroyant
    la présente Licence publique et les droits accordés par elle.]{#s1j}
k.  [**Partager** signifie mettre une œuvre à la disposition du public
    par tout moyen ou procédé qui requiert l'autorisation découlant des
    Droits accordés par la licence, tels que les droits de reproduction,
    de représentation au public, de distribution, de diffusion, de
    communication ou d'importation, y compris de manière à ce que chacun
    puisse y avoir accès de l'endroit et au moment qu'il choisit
    individuellement.]{#s1k}
l.  [**Droit sui generis des producteurs de bases de données** signifie
    droits distincts du droit d\'auteur résultant de la Directive
    96/9/CE du Parlement européen et du Conseil du 11 mars 1996 sur la
    protection juridique des bases de données, ainsi que tout autre
    droit de nature équivalente dans le monde.]{#s1l}
m.  [**Vous** (preneur de licence) se rapporte à tout individu ou entité
    exerçant les Droits accordés par la licence. **Votre** et **Vos**
    renvoient également au preneur de licence.]{#s1m}

**Article 2 - Champ d'application de la présente Licence publique.**

a.  [**Octroi de la licence**.]{#s2a}
    1.  [Sous réserve du respect des termes et conditions d\'utilisation
        de la présente Licence publique, le Donneur de licence Vous
        autorise à exercer pour le monde entier, à titre gratuit, non
        sous-licenciable, non exclusif, irrévocable, les Droits accordés
        par la licence afin de :]{#s2a1}
        A.  [reproduire et Partager l'Œuvre sous licence, en tout ou
            partie ; et]{#s2a1A}
        B.  [produire, reproduire et Partager l'Œuvre dérivée.]{#s2a1B}
    2.  [[Exceptions et
        limitations]{style="text-decoration: underline;"}. Afin de lever
        toute ambiguïté, lorsque les Exceptions et limitations
        s'appliquent à Votre utilisation, la présente Licence publique
        ne s'applique pas et Vous n'avez pas à Vous conformer à ses
        termes et conditions.]{#s2a2}
    3.  [[Durée]{style="text-decoration: underline;"}. La durée de la
        présente Licence publique est définie à l'Article
        [6(a)](#s6a).]{#s2a3}
    4.  [[Supports et formats : modifications techniques
        autorisées]{style="text-decoration: underline;"}. Le Donneur de
        licence Vous autorise à exercer les Droits accordés par la
        licence sur tous les supports et formats connus ou encore
        inconnus à ce jour, et à apporter toutes les modifications
        techniques que ceux-ci requièrent. Le Donneur de licence renonce
        et/ou accepte de ne pas exercer ses droits qui pourraient être
        susceptibles de Vous empêcher d'apporter les modifications
        techniques nécessaires pour exercer les Droits accordés par la
        licence, y compris celles nécessaires au contournement des
        Mesures techniques efficaces. Dans le cadre de la présente
        Licence publique, le fait de ne procéder qu'à de simples
        modifications techniques autorisées selon les termes du présent
        Article [2(a)(4)](#s2a4) n'est jamais de nature à créer une
        Œuvre dérivée.]{#s2a4}
    5.  [[Utilisateurs en
        aval.]{style="text-decoration: underline;"}]{#s2a5}
        ::: {.para}
        A.  [[Offre du Donneur de licence -- Œuvre sous
            licence]{style="text-decoration: underline;"}. Chaque
            utilisateur de l'Œuvre sous licence reçoit automatiquement
            une offre de la part du Donneur de licence lui permettant
            d'exercer les Droits accordés par la licence selon les
            termes et conditions de la présente Licence
            publique.]{#s2a5A}
        B.  [[Offre additionnelle du Donneur de licence -- Œuvre
            dérivée.]{style="text-decoration: underline;"} Chaque
            utilisateur d'une Œuvre dérivée reçoit automatiquement une
            offre du Donneur de licence lui permettant d'exercer les
            Droits accordés par la licence sur l'Œuvre dérivée selon les
            termes et conditions de la Licence d'Œuvre dérivée que Vous
            appliquez.]{#s2a5B}
        C.  [[Pas de restrictions en aval pour les utilisateurs
            suivants]{style="text-decoration: underline;"}. Vous ne
            pouvez proposer ou imposer des termes et conditions
            supplémentaires ou différents, ou appliquer quelque Mesure
            technique efficace que ce soit à l'Œuvre sous licence si
            ceux(celles)-ci sont de nature à restreindre l'exercice des
            Droits accordés par la licence aux utilisateurs de l'Œuvre
            sous licence.]{#s2a5C}
        :::
    6.  [[Non approbation]{style="text-decoration: underline;"}. Aucun
        élément de la présente Licence publique ne peut être interprété
        comme laissant supposer que le preneur de licence ou que
        l'utilisation qu'il fait de l'Œuvre sous licence est lié à,
        parrainé, approuvé, ou doté d\'un statut officiel par le Donneur
        de licence ou par toute autre personne à qui revient
        l'attribution de l'Œuvre sous licence, comme indiqué à l'Article
        [3(a)(1)(A)(i)](#s3a1Ai).]{#s2a6}

b.  ::: {#s2b}
    **Autres droits**.

    1.  [Les droits moraux, tel que le droit à l'intégrité de l'œuvre,
        ne sont pas accordés par la présente Licence publique, ni le
        droit à l'image, ni le droit au respect de la vie privée, ni
        aucun autre droit de la personnalité ou apparenté ; cependant,
        dans la mesure du possible, le Donneur de licence renonce et/ou
        accepte de ne pas faire valoir les droits qu'il détient de
        manière à Vous permettre d'exercer les Droits accordés par la
        licence.]{#s2b1}
    2.  [Le droit des brevets et le droit des marques ne sont pas
        concernés par la présente Licence publique.]{#s2b2}
    3.  [Dans la mesure du possible, le Donneur de licence renonce au
        droit de collecter des redevances auprès de Vous pour l'exercice
        des Droits accordés par la licence, directement ou indirectement
        dans le cadre d'un régime de gestion collective facultative ou
        obligatoire assorti de possibilités de renonciation quel que
        soit le type d'accord ou de licence. Dans tous les autres cas,
        le Donneur de licence se réserve expressément le droit de
        collecter de telles redevances.]{#s2b3}
    :::

**Article 3 -- Conditions d\'utilisation de la présente Licence
publique.**

L'exercice des Droits accordés par la licence est expressément soumis
aux conditions suivantes.

a.  ::: {#s3a}
    **Attribution**.

    1.  ::: {#s3a1}
        Si Vous partagez l'Œuvre sous licence (y compris sous une forme
        modifiée), Vous devez :

        A.  [conserver les informations suivantes lorsqu'elles sont
            fournies par le Donneur de licence avec l'Œuvre sous licence
            :]{#s3a1A}
            i.  [identification du(des) auteur(s) de l'Œuvre sous
                licence et de toute personne à qui revient l'attribution
                de l'Œuvre sous licence, dans la mesure du possible,
                conformément à la demande du Donneur de licence (y
                compris sous la forme d'un pseudonyme s'il est indiqué)
                ;]{#s3a1Ai}
            ii. [l'indication de l'existence d'un droit d'auteur
                ;]{#s3a1Aii}
            iii. [une notice faisant référence à la présente Licence
                 publique ;]{#s3a1Aiii}
            iv. [une notice faisant référence aux limitations de
                garantie et exclusions de responsabilité ;]{#s3a1Aiv}
            v.  [un URI ou un hyperlien vers l'Œuvre sous licence dans
                la mesure du possible ;]{#s3a1Av}
        B.  [Indiquer si Vous avez modifié l'Œuvre sous licence et
            conserver un suivi des modifications précédentes ;
            et]{#s3a1B}
        C.  [Indiquer si l'Œuvre sous licence est mise à disposition en
            vertu de la présente Licence publique en incluant le texte,
            l'URI ou l'hyperlien correspondant à la présente Licence
            publique.]{#s3a1C}
        :::

    2.  [Vous pouvez satisfaire aux conditions de l'Article
        [3(a)(1)](#s3a1) dans toute la mesure du possible, en fonction
        des supports, moyens et contextes dans lesquels Vous Partagez
        l'Œuvre sous licence. Par exemple, Vous pouvez satisfaire aux
        conditions susmentionnées en fournissant l'URI ou l'hyperlien
        vers la ressource incluant les informations requises.]{#s3a2}

    3.  [Bien que requises aux termes de l'Article [3(a)(1)(A)](#s3a1A),
        certaines informations devront être retirées, dans la mesure du
        possible, si le Donneur de licence en fait la demande.]{#s3a3}
    :::

b.  [**Partage dans les mêmes conditions**.]{#s3b}

    Outre les conditions indiquées à l'Article [3(a)](#s3a), si Vous
    Partagez une Œuvre dérivée que Vous avez réalisée, les conditions
    suivantes s'appliquent aussi.

    1.  [La Licence d'Œuvre dérivée que Vous appliquez doit être une
        licence Creative Commons avec les mêmes Eléments de licence,
        qu'il s'agisse de cette version ou d'une version ultérieure, ou
        une Licence compatible BY-SA.]{#s3b1}
    2.  [Vous devez inclure le texte, l'URI ou l'hyperlien correspondant
        à la Licence d'Œuvre dérivée que Vous appliquez. Ces conditions
        peuvent être satisfaites dans la mesure du raisonnable suivant
        les supports, moyens et contextes via lesquels Vous Partagez
        l'Œuvre dérivée.]{#s3b2}
    3.  [Vous ne pouvez pas proposer ou imposer des termes ou des
        conditions supplémentaires ou différents ou appliquer des
        Mesures techniques efficaces à l'Œuvre dérivée qui seraient de
        nature à restreindre l'exercice des Droits accordés par la
        Licence d'Œuvre dérivée que Vous appliquez.]{#s3b3}

**Article 4 -- Le Droit sui generis des producteurs de bases de
données.**

Lorsque les Droits accordés par la licence incluent le Droit sui generis
des producteurs de bases de données applicable à Votre utilisation de
l'Œuvre sous licence :

a.  [afin de lever toute ambiguïté, l'Article [2(a)(1)](#s2a1) Vous
    accorde le droit d'extraire, réutiliser, reproduire et Partager la
    totalité ou une partie substantielle du contenu de la base de
    données ;]{#s4a}
b.  [si Vous incluez la totalité ou une partie substantielle du contenu
    de la base de données dans une base de données pour laquelle Vous
    détenez un Droit sui generis de producteur de bases de données, la
    base de données sur laquelle Vous détenez un tel droit (mais pas ses
    contenus individuels) sera alors considérée comme une Œuvre dérivée,
    y compris pour l'application de l'Article [3(b)](#s3b) ; et]{#s4b}
c.  [Vous devez respecter les conditions de l'Article [3(a)](#s3a) si
    Vous Partagez la totalité ou une partie substantielle du contenu des
    bases de données.]{#s4c}

Afin de lever toute ambiguïté, le présent Article [4](#s4) complète mais
ne remplace pas Vos obligations découlant des termes de la présente
Licence publique lorsque les Droits accordés par la licence incluent
d'autres Droit d'auteur et droits connexes.

**Article 5 -- Limitations de garantie et exclusions de
responsabilité.**

a.  [**Sauf indication contraire et dans la mesure du possible, le
    Donneur de licence met à disposition l'Œuvre sous licence telle
    quelle, et n'offre aucune garantie de quelque sorte que ce soit,
    notamment expresse, implicite, statutaire ou autre la concernant.
    Cela inclut, notamment, les garanties liées au titre, à la valeur
    marchande, à la compatibilité de certaines utilisations
    particulières, à l'absence de violation, à l'absence de vices cachés
    ou autres défauts, à l'exactitude, à la présence ou à l'absence
    d'erreurs connues ou non ou susceptibles d'être découvertes dans
    l'Œuvre sous licence. Lorsqu'une limitation de garantie n'est pas
    autorisée en tout ou partie, cette clause peut ne pas Vous être
    applicable.**]{#s5a}
b.  [**Dans la mesure du possible, le Donneur de licence ne saurait voir
    sa responsabilité engagée vis-à-vis de Vous, quel qu'en soit le
    fondement juridique (y compris, notamment, la négligence), pour tout
    préjudice direct, spécial, indirect, incident, conséquentiel,
    punitif, exemplaire, ou pour toutes pertes, coûts, dépenses ou tout
    dommage découlant de l'utilisation de la présente Licence publique
    ou de l'utilisation de l'Œuvre sous licence, même si le Donneur de
    licence avait connaissance de l'éventualité de telles pertes, coûts,
    dépenses ou dommages. Lorsqu'une exclusion de responsabilité n'est
    pas autorisée en tout ou partie, cette clause peut ne pas Vous être
    applicable.**]{#s5b}

```{=html}
<!-- -->
```
c.  [Les limitations de garantie et exclusions de responsabilité
    ci-dessus doivent être interprétées, dans la mesure du possible,
    comme des limitations et renonciations totales de toute
    responsabilité.]{#s5c}

**Article 6 -- Durée et fin.**

a.  [La présente Licence publique s'applique pendant toute la durée de
    validité des Droits accordés par la licence. Cependant, si Vous
    manquez à Vos obligations prévues par la présente Licence publique,
    Vos droits accordés par la présente Licence publique seront
    automatiquement révoqués.]{#s6a}

b.  ::: {#s6b}
    Lorsque les Droits accordés par la licence ont été révoqués selon
    les termes de l'Article [6(a)](#s6a), ils seront rétablis :

    1.  [automatiquement, à compter du jour où la violation aura cessé,
        à condition que Vous y remédiiez dans les 30 jours suivant la
        date à laquelle Vous aurez eu connaissance de la violation ;
        ou]{#s6b1}
    2.  [à condition que le Donneur de licence l'autorise
        expressément.]{#s6b2}

    Afin de lever toute ambiguïté, le présent Article [6(b)](#s6b)
    n'affecte pas le droit du Donneur de licence de demander réparation
    dans les cas de violation de la présente Licence publique.
    :::

c.  [Afin de lever toute ambiguïté, le Donneur de licence peut également
    proposer l'Œuvre sous licence selon d'autres termes et conditions et
    peut cesser la mise à disposition de l'Œuvre sous licence à tout
    moment ; une telle cessation n'entraîne pas la fin de la présente
    Licence publique.]{#s6c}

d.  [Les Articles [1](#s1), [5](#s5), [6](#s6), [7](#s7), et [8](#s8)
    continueront à s'appliquer même après la résiliation de la présente
    Licence publique.]{#s6d}

**Article 7 -- Autres termes et conditions.**

a.  [Sauf accord exprès, le Donneur de licence n'est lié par aucune
    modification des termes de Votre part.]{#s7a}
b.  [Tous arrangements, ententes ou accords relatifs à l'Œuvre sous
    licence non mentionnés dans la présente Licence publique sont
    séparés et indépendants des termes et conditions de la présente
    Licence publique.]{#s7b}

**Article 8 -- Interprétation.**

a.  [Afin de lever toute ambiguïté, la présente Licence publique ne doit
    en aucun cas être interprétée comme ayant pour effet de réduire,
    limiter, restreindre ou imposer des conditions plus contraignantes
    que celles qui sont prévues par les dispositions légales
    applicables.]{#s8a}
b.  [Dans la mesure du possible, si une clause de la présente Licence
    publique est déclarée inapplicable, elle sera automatiquement
    modifiée a minima afin de la rendre applicable. Dans le cas où la
    clause ne peut être modifiée, elle sera écartée de la présente
    Licence publique sans préjudice de l'applicabilité des termes et
    conditions restants.]{#s8b}
c.  [Aucun terme ni aucune condition de la présente Licence publique ne
    sera écarté(e) et aucune violation ne sera admise sans l'accord
    exprès du Donneur de licence.]{#s8c}
d.  [Aucun terme ni aucune condition de la présente Licence publique ne
    constitue ou ne peut être interprété(e) comme une limitation ou une
    renonciation à un quelconque privilège ou à une immunité
    s'appliquant au Donneur de licence ou à Vous, y compris lorsque
    celles-ci émanent d'une procédure légale, quel(le) qu'en soit le
    système juridique concerné ou l'autorité compétente.]{#s8d}

Creative Commons n'est pas partie prenante de ses licences publiques.
Néanmoins, Creative Commons se réserve le droit d\'utiliser une de ses
licences publiques pour les œuvres qu'elle publie, et dans ce cas sera
considérée comme « Donneur de licence ». Le texte des licences publiques
Creative Commons est versé au domaine public en vertu de [CC0 Domaine
Public](//creativecommons.org/publicdomain/zero/1.0/legalcode.fr). A
l'exception des seuls cas où il est indiqué que l'œuvre est mise à
disposition sous licence publique Creative Commons et ceux autorisés par
les statuts de Creative Commons disponibles sur
[creativecommons.org/policies](//creativecommons.org/policies), Creative
Commons n'autorise l'utilisation par aucune partie de la marque
\"Creative Commons\" ou de toute autre marque ou logo de Creative
Commons sans le consentement écrit préalable de Creative Commons. Cette
restriction relative à l\'utilisation des marques ne constitue pas une
partie de nos licences publiques.\
\
Vous pouvez joindre Creative Commons via
[creativecommons.org](//creativecommons.org/).

[Autres langues disponibles]{#languages} :
[العربية](/licenses/by-sa/4.0/legalcode.ar),
[čeština](/licenses/by-sa/4.0/legalcode.cs),
[Deutsch](/licenses/by-sa/4.0/legalcode.de),
[Ελληνικά](/licenses/by-sa/4.0/legalcode.el),
[English](/licenses/by-sa/4.0/legalcode),
[Español](/licenses/by-sa/4.0/legalcode.es),
[euskara](/licenses/by-sa/4.0/legalcode.eu),
[suomeksi](/licenses/by-sa/4.0/legalcode.fi),
[hrvatski](/licenses/by-sa/4.0/legalcode.hr), [Bahasa
Indonesia](/licenses/by-sa/4.0/legalcode.id),
[italiano](/licenses/by-sa/4.0/legalcode.it),
[日本語](/licenses/by-sa/4.0/legalcode.ja),
[한국어](/licenses/by-sa/4.0/legalcode.ko),
[Lietuvių](/licenses/by-sa/4.0/legalcode.lt),
[latviski](/licenses/by-sa/4.0/legalcode.lv), [te reo
Māori](/licenses/by-sa/4.0/legalcode.mi),
[Nederlands](/licenses/by-sa/4.0/legalcode.nl),
[norsk](/licenses/by-sa/4.0/legalcode.no),
[polski](/licenses/by-sa/4.0/legalcode.pl),
[português](/licenses/by-sa/4.0/legalcode.pt),
[română](/licenses/by-sa/4.0/legalcode.ro),
[русский](/licenses/by-sa/4.0/legalcode.ru),
[Slovenščina](/licenses/by-sa/4.0/legalcode.sl),
[svenska](/licenses/by-sa/4.0/legalcode.sv),
[Türkçe](/licenses/by-sa/4.0/legalcode.tr),
[українська](/licenses/by-sa/4.0/legalcode.uk),
[中文](/licenses/by-sa/4.0/legalcode.zh-Hans),
[華語](/licenses/by-sa/4.0/legalcode.zh-Hant). Veuillez lire la
[FAQ](/faq/#officialtranslations) pour plus d\'informations sur les
traductions officielles.
