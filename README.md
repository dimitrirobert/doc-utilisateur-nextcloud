# Documentation utilisateur NextCloud

Documentation en français de NextCloud orientée utilisateurs.
Elle met l'accent sur les cas d'usage et l'utilisation par des organisations.

Elle est actuellement prévue pour un affichage sur écran (certaines images sont des gif animés) mais pourra être assez facilement adaptée pour une impression.

Le dossier images contient les images intégrées à la documentation. Le dossier sources contient les documents de travail nécessaire à l'élaboration de cette documentation, notamment les images XCF permettant de rendre *imprimables* les animations.

La documentation est lisible en consultant le fichier [doc.md].

## Création de la version HTML avec Pandoc

```
pandoc -s -i doc.md -o doc.html --toc --toc-depth 4 --template=./templates/default.htmlcoalition
```

