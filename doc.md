---
lang: fr-FR
to: html5
output-file: DocumentationNextcloud.html
title: Nextcloud pour s'organiser
toc-depth: 3
css:
- css/lacoalition.css

...

<!-- # Nextcloud pour s'organiser -->

## Introduction

### Que faire avec Nextcloud ?

Depuis la version 20, Nextcloud se targue d'être une plateforme de collaboration. En effet, à la base Nextcloud était un logiciel libre de *cloud*. Selon Wikipédia, Le *cloud computing* (en français l'informatique en nuage), correspond à l’accès à des services informatiques (serveurs, stockage, mise en réseau, logiciels) via Internet (le « cloud » ou « nuage ») à partir d’un fournisseur.

Nextcloud propose un ensemble d'applications sous formes de modules que vous pouvez greffer sur l'installation de base, faisant de Nextcloud un véritable espace de travail en groupe et en ligne.

Les modules de base, installés d'office sont :

* **Fichiers** : [stockage](#gérez-vos-documents-en-ligne) et partage de documents^[Historiquement on parle de fichier, mais le terme de document est plus répandu aujourd'hui. Ces deux termes désignent la même chose.] via le protocole WebDAV^[dérivé de HTTP ce protocole permet l'accès en lecture et écriture à des dossiers et documents via le Web.]. [Synchronisation](#synchronisation) possible via un logiciel client sur ordinateur et téléphone.
* **Images** : une présentation sous forme de galerie les images stockées via le module Fichiers.
* **Agenda** : un système de calendriers partageables. Les agendas peuvent être gérés par un logiciel tiers ([Thunderbird](https://fr.wikipedia.org/wiki/Mozilla_Thunderbird) ou une application d'agenda sur téléphone) via le protocole CalDAV^[dérivé de WebDAV, à ne pas confondre avec iCalendar.].
* **Tâches** : un gestionnaire de tâches qui utilise également le protocole CalDAV.
* **Contacts** : un système de gestion de carnets de contacts. Les contacts peuvent être gérés par un logiciel tiers via le protocole CardDAV^[dérivé de WebDAV également.].
* **Discussions** : un espace de visio-conférence avec également discussion par écrit (*tchat*) et partage d'écran. Ce module utilise le protocole WebRTC^[Web Real-Time Communication, littéralement « communication en temps réel pour le Web », un protocole permettant de faire transiter du son et de la vidéo via le Web], tout comme [Jitsi Meet](https://fr.wikipedia.org/wiki/Jitsi) et [BigBlueButton](https://fr.wikipedia.org/wiki/BigBlueButton).
* **Deck** : un système de gestion de projets de type [Kanban](https://fr.wikipedia.org/wiki/Kanban).

Tous ces modules peuvent communiquer entre-eux et partager des informations. De même, les données traitées peuvent être partagées avec d'autres membres inscrits sur le même Nextcloud.

On trouve également des applications de bureautique ou de simple rédaction de documents :

* prendre des notes à l'aide d'un *pad*^[Framapad est le service de pad le plus célèbre et le plus utilisé : <https://framapad.org/>] ;
* prendre des notes aux formats texte simple (dit *.txt*) et [Markdown](https://fr.wikipedia.org/wiki/Markdown) ;
* utiliser une suite bureautique : [OnlyOffice](https://fr.wikipedia.org/wiki/Onlyoffice) ou [Collabora Online](https://www.collaboraoffice.com/fr/code/#what_is_code).

### Pourquoi préférer Nextcloud

Il existe plusieurs services de *cloud* connus et répandus, tous proposés par des *GAFAM*^[Terme désignant des multinationales du numérique fondant tout ou partie de leurs revenus sur le commerce des données personnelles. L'acronyme cible Google, Apple, Facebook, Amazon et Microsoft mais ce ne sont pas les seules.] tels que Google Drive, Microsoft OneDrive, Dropbox, iCloud, Amazon Drive. Ces services, tous centralisés et souvent hébergés aux États-Unis (donc couverts par le [Patriot Act](https://fr.wikipedia.org/wiki/USA_PATRIOT_Act)) reposent sur l'exploitation des données qui y sont stockées. L'utilisateur, en acceptant les conditions générales d'utilisation, laisse ces hébergeurs utiliser des données privées et/ou professionnelles en vue d'une exploitation commerciale.

De plus, les lois de certains gouvernements (comme les États-Unis avec le Patriot Act) permettent aux autorités d'avoir accès aux données très (trop ?) facilement sans même en avertir les propriétaires.

Si vous doutez de l'importance stratégique de vos données vous devriez consulter le site de [Nothing 2 hide](http://nothing2hide.org/fr/) (« rien à cacher ») pour en apprendre plus.

En quoi Nextcloud est-il différent ?

* C'est un logiciel libre dont le code source est diffusé et donc auditable par des développeurs indépendant de l'éditeur.
* C'est surtout un logiciel que vous pouvez installer (ou faire installer) sur un serveur de confiance. Vous gardez ainsi la main sur vos données !

## Premiers pas dans Nextcloud

### Validez votre inscription

Lorsque qu'un administrateur d'une instance^[Nous appelons instance un exemplaire (une copie) du logiciel Nextcloud installé sur un serveur Web. La force de Nextcloud étant que vous pouvez télécharger le logiciel, l'installer sur votre hébergement Web et ainsi proposer le service Nextcloud] Nextcloud vous crée un compte (un accès à l'instance), vous recevez un email vous en informant et vous invitant à vous choisir un mot de passe.

Il se peut que l'email soit en anglais. Dans ce cas, son sujet est : **Your Nextcloud de LA Coalition account was created** où « Nextcloud de LA Coalition » est ici le nom de l'instance. De ce fait, il se peut aussi que l'email ait été classé en spam par votre messagerie. N'hésitez pas à consulter votre dossier d'indésirables si l'on affirme vous avoir inscrit mais que vous n'avez apparemment rien reçu.

L'email de bienvenue se présente comme ceci :

![](./images/20201111-120953.png)

Le lien important est **Set your password** pour définir votre mot de passe^[En effet, l'administrateur de l'instance n'a pas à connaître votre mot de passe et a la possibilité de vous laisser le soin de le définir vous-même.].

Cela vous envoie sur l'instance Nextcloud où vous ne pouvez rien faire d'autre à part définir un mot de passe. Une fois cette étape effectuée, il ne vous reste qu'à vous connecter.

![](./images/20201111-123854.png)

### Première connexion

Lorsque vous vous connectez pour la première fois, vous êtes accueilli par une bannière de présentation de Nextcloud. Vous pouvez la visionner ou cliquer sur la croix pour la fermer.

![](./images/banniere_accueil.png)

Une fois fermée cette bannière, vous pouvez admirer le tableau de bord, la page d'accueil de Nextcloud depuis la version 20.

![](./images/tableau_de_bord1.png)

Dans un premier temps repérez les icônes des applications disponibles. Toutes les instances Nextcloud ne proposent pas les mêmes applications, mais la [base](#que-faire-avec-nextcloud) est toujours disponible. Passez la souris sur la barre d'icônes pour voir leur nom apparaître.

![](./images/icones-applications.mp4)

Le tableau de bord est votre point d'entrée dans Nextcloud. Outre les différentes applications, vous pouvez :

* [Personnaliser le tableau de bord]() en choisissant les widgets que vous souhaitez voir en priorité ;
* [Effectuer une recherche]() dans toutes les applications (noms de fichier, discussion, contacts, agenda, etc.) ;
* [Recevoir des notifications]() des événements vous concernant ;
* [Chercher un contact]() parmi les autres personnes présentes sur la même instance que vous ;
* Ouvrir votre menu personnel pour [modifier vos paramètres](#personnalisez-les-paramètres-de-base) ou vous déconnecter ;
* Avoir un mini bulletin météo ;
* Choisir le statut signalant votre disponibilité.

> #### Que faire si Nextcloud est en anglais ?
> 
> Premièrement, prévenez votre administrateur (ou la personne vous ayant invité) que la langue est mal configurée. L'administrateur doit ajouter la ligne suivante dans le fichier `config.php`.
> ```
> "default_language" => "fr"
> ```
> Deuxièmement, vous devez changer la langue dans vos paramètres. Cliquez sur l'icône ronde colorée portant vos initiales dans le coin supérieur droit de la page de Nextcloud et choisissez *Settings* (paramètres en anglais).
> 
> ![](./images/acces_settings.png)
> 
> Puis changez les paramètres *Language* et *Locale*. Le paramètre *Locale* désigne les paramètres régionaux. Vous devez choisir *French* pour la France et ainsi avoir la bonne heure locale, le bon début de semaine.
> 
> ![](./images/changer_langue.png)
> 
> L'enregistrement des paramètres est automatique, vous pouvez reprendre vos activités.

### Personnalisez les paramètres de base

Rendez-vous dans les paramètres, via votre menu personnel en haut à droite de la fenêtre.

Vérifiez que les informations personnelles de base sont bien présentes.

![](./images/parametres-infos-perso.png)

1. Une adresse email est nécessaire pour réinitialiser votre mot de passe et éventuellement recevoir des notifications.
2. Vous pouvez déposer une photo qui sera affichée sous forme de pastille dans certaines applications, comme le *chat* (discussion) ou les partages de documents. Vous pouvez également utiliser une image déjà présente dans votre dépôt de fichiers.
3. Assurez-vous que la langue et les paramètres régionaux sont bons.
4. Vous pouvez parcourir les [autres paramètres]().

> #### Attention à votre bloqueur de publicités
> 
> Vous utilisez un bloqueur de publicité tel que **uBlock Origin** et c'est très bien. Cependant, selon les filtres que vous avez choisi, il peut bloquer certaines fonctionnalités de Nextcloud.
> Aussi vous devriez désactiver votre bloqueur de publicités pour Nextcloud (et seulement pour Nextcloud).
> 
> Nextcloud est un outil de travail normalement hébergé sur un serveur de confiance : la publicité n'y a pas sa place. Si d'aventure vous étiez amenés à utiliser un service Nextcloud avec de la publicité, changez-en. La publicité est gérée par des régies externes et leur intrusion dans un outil de travail peut être source de fuites de données vers une destination inconnue et malveillante.

## Gérez vos documents en ligne

C'est l'utilisation de base de Nextcloud, le service de *cloud*, c'est-à-dire, de dépôt de dossiers et documents en ligne. Ces documents sont alors accessibles depuis plusieurs ordinateurs ou téléphones.

> Il est même possible de synchroniser les données entre le serveur Nextcloud et vos différents appareils. Tout cela est expliqué au chapitre sur la [synchronisation](#synchronisation).

### L'interface

Tour d'horizon rapide.

![](./images/20200821-123024.png)

1. En haut à gauche, à côté du logo, se trouve les boutons pour accéder directement à certains modules. Passez la souris dessus pour voir leur nom. Ici, nous sommes sur le module **Fichiers** (une flèche apparaît sous l'icône et les autres sont légèrement grisées).
2. La partie gauche vous donne accès à certains filtres. *Tous les fichiers* vous permet de parcourir votre arborescence.
3. Le contenu du dossier courant est affiché ici sous forme de liste.
	* La case à cocher en tête de ligne permet de sélectionner un (ou plusieurs) document ou dossier.
	* Cliquez sur le nom d'un dossier ou d'un document pour l'ouvrir.
	* Au bout du nom, aligné à droite, se trouvent deux boutons. Le premier pour partager le document, le second (trois points) pour ouvrir un menu d'actions.
	* Puis viennent la taille (poids) et la date de dernière modification.
4. Le bouton **+** permet de déposer des documents ou de créer un nouveau dossier.
5. En bas à gauche se trouvent :
	* l'accès à la corbeille ;
	* l'état d'occupation de votre espace en fonction de votre quota ;
	* un menu de paramètres (présent sur tous les modules).

### Créer un nouveau dossier

* Cliquez sur le **+** pour ouvrir le menu.
* Choisissez *Nouveau dossier*.
* Définissez un nom.
* Validez en cliquant sur la flèche.
* Le dossier apparaît ainsi que son panneau d'informations.
* Cliquez sur le nom pour entrer dans le nouveau dossier.

![](./images/nouveau-dossier.gif)

### Déposer des documents

* Cliquez sur le **+** pour ouvrir le menu.
* Choisissez *Envoyer un fichier*.
* La fenêtre *Ouvrir un fichier* de votre navigateur s'ouvre et vous pouvez ainsi parcourir vos données locales (sur votre ordinateur). Sélectionnez le ou les fichiers (pressez la touche *Ctrl* pour sélectionner plusieurs fichiers) puis validez.
* Patientez pendant l'envoi.

![](./images/depot-documents.gif)

### Déplacer un document

#### Déplacer un seul document par le menu

* Cliquez sur l'icône à trois points^[dite icône de menu « boulettes de viande » (ou *meatballs menu* en anglais)] pour ouvrir le menu. ![](./images/icone-meatballs.png)
* Choisissez *Déplacer ou copier*.
* Vous pouvez ensuite parcourir l'arborescence de votre espace Nextcloud (l'icône « maison » représente la racine de votre dossier personnel). ![](./images/icone-maison.png)
* Choisissez le dossier de destination.
* Choisissez de copier ou de déplacer.
* Patientez quelques secondes.

![](./images/deplacer-1-document.gif)

#### Déplacer plusieurs documents par le menu

* Cochez les fichiers à copier ou déplacer.
* Ouvrez le menu *Actions* dont le bouton apparaît dès qu'un fichier ou dossier est sélectionné.
* Choisissez *Déplacer ou copier*.
* Vous pouvez ensuite parcourir l'arborescence de votre espace Nextcloud (l'icône « maison » représente la racine de votre dossier personnel).
* Choisissez le dossier de destination.
* Choisissez de copier ou de déplacer.
* Patientez quelques secondes.

![](./images/deplacer-documents-menu.gif)

#### Déplacer par cliqué-glissé

* Cochez les fichiers à copier ou déplacer.
* Cliquez sur l'un des fichiers puis déplacez la souris sans lâcher le bouton gauche. Déposez les fichiers sur le dossier de votre choix (vous pouvez les déposer sur un dossier parent via la barre de chemin en haut de la liste des fichiers).

![](./images/deplacer-documents-glisser.gif)

### Effacez des fichiers

Effacez un fichier ou un dossier via son menu *meatballs* ![](./images/icone-meatballs.png) en choisissant *Supprimer*.

Si vous souhaitez supprimer plusieurs fichiers et dossiers à la fois, il faut les sélectionner au préalable (en les cochant), puis choisir *Actions > Supprimer*.

![](./images/supprimer-fichiers.mp4)

#### Restaurez vos fichiers supprimés

Nextcloud dispose d'une corbeille. Si vous avez supprimé des fichiers et des dossiers vous aurez un lien *Fichiers supprimés* dans la colonne de gauche dans l'application *Fichiers* (1). C'est l'accès à la corbeille.

Affichez le contenu de la corbeille et cochez les fichiers que vous souhaitez restaurer (2), le bouton *Actions* donne accès à la fonction, ou cliquez directement sur le bouton *Restaurer* (3) si vous ne voulez récupérer qu'un fichier.

![](./images/restaurer-fichiers.png)

### Classez avec des étiquettes

> #### À quoi servent les étiquettes ?
> Les étiquettes (*tags* en anglais) permettent une méthode de classement transversale à celle des dossiers. En effet, le classement en dossier implique une arborescence et un fichier ne peut se trouver dans deux dossiers différents à moins d'être dupliqué. La racine de cet arbre est votre dossier personnel, les branches sont constituées des dossiers et sous-dossiers et les fichiers sont les feuilles.
>
> Une utilisation évidente des étiquettes concerne les photos. Par exemple, si vous classez vos photos dans des dossiers par événements (ou par occasion de prise de photos) vous pouvez utiliser les étiquettes pour caractériser le contenu des photos. Exemples : une étiquette par personne photographiée, dans quelle ville la photo a été prise, en extérieur ou en intérieur, y a-t-il un chat sur la photo, etc. Cela vous permet ensuite de retrouver facilement toutes les photos prises dans telle ville ou comportant un chat.
> 
> ![](./images/principe-etiquettes.png)

Ajouter une étiquette à un fichier (pas forcément une photo) ou un dossier nécessite quelques étapes (on peut espérer qu'une prochaine version réduise le nombre de clics) :

1. ouvrez le menu du fichier que vous voulez étiqueter
2. choisissez *Détails* pour afficher le panneau de détail à droite
3. cliquez sur l'icône *meatballs* pour ouvrir un nouveau menu
4. choisissez enfin *Étiquettes*
5. saisissez votre étiquette et/ou choisissez parmi les étiquettes déjà créées

**Important** : évitez de créer des doublons avec une syntaxe légèrement différente (avec ou sans accent), regardez toujours les étiquettes existantes avant d'en créer une nouvelle.

![](./images/ajouter-etiquette.mp4)

Parcourez ensuite vos fichiers par étiquette. Dans la partie navigation (colonne de gauche) de l'application *Fichiers* vous voyez désormais apparaître l'entrée *Étiquettes*.	 Choisissez-la.

Une zone de saisie apparaît dans la partie centrale. Vous pouvez commencer à saisir une étiquette et choisir la bonne dans la liste déroulante. Vous pouvez choisir plusieurs étiquettes. N'apparaîtront alors que les fichiers possédant toutes les étiquettes sélectionnées.

Effacez une étiquette avec la touche *Effacement arrière* (au dessus de la touche *Entrée* sur un clavier français).

![](./images/parcourir-etiquettes.mp4)

## Partagez vos documents

### Partagez avec des membres

Contrairement à la copie, le partage ne duplique pas le fichier ou le dossier partagé. Il reste la propriété de la personne qui le partage.

Si un fichier est partagé avec autorisation de modification, alors les destinataires pourront modifier le fichier ou le contenu du dossier.

Lorsqu'un dossier est partagé, tout son contenu l'est également.

#### Partagez un fichier

* Dans la liste des fichiers, cliquez sur l'icône de partage du fichier que vous souhaitez partager. ![](./images/icone-partage.png)
* La fenêtre *Détails* s'ouvre sur la partie droite de l'écran.
* Dans le champ de saisie *Nom, ID du cloud fédéré ou adresse e-mail…* commencez à saisir le nom de la personne à qui vous souhaitez votre document.
* Nextcloud vous propose un ou plusieurs choix, cliquez sur le bon.
* Le destinataire de votre partage apparaît alors dans la fenêtre *Détails*. Un bouton « boulettes de viande » vous permet de préciser les paramètres de partage. ![](./images/icone-meatballs-cercle.png)
	- *Autoriser la modification* : le destinataire peut modifier le contenu du fichier (mais pas le supprimer).
	- *Autoriser le repartage* : le destinataire peut le partager à un autre membre.
	- *Définir une date d'expiration* : le partage sera automatiquement rompu passée cette date.
	- *Note au destinataire* : précisez une courte information.
	- *Ne plus partager* : rompez le partage de ce fichier.
	
![](./images/partage-fichier.gif)

#### Partagez un dossier

* Dans la liste des fichiers, cliquez sur l'icône de partage du dossier que vous souhaitez partager. ![](./images/icone-partage.png)
* La fenêtre *Détails* s'ouvre sur la partie droite de l'écran.
* Dans le champ de saisie *Nom, ID du cloud fédéré ou adresse e-mail…* commencez à saisir le nom de la personne à qui vous souhaitez votre dossier.
* Nextcloud vous propose un ou plusieurs choix, cliquez sur le bon.
* Le destinataire de votre partage apparaît alors dans la fenêtre *Détails*. Un bouton « boulettes de viande » vous permet de préciser les paramètres de partage. ![](./images/icone-meatballs-cercle.png)
	- *Autoriser la modification* : le destinataire peut modifier le contenu du dossier (mais pas le supprimer).
	- *Autoriser la création* : le destinataire peut créer de nouveaux fichiers ou dossiers dans le dossier partagé.
	- *Autoriser la suppression* : le destinataire peut supprimer des éléments contenus dans le dossier partagé (peut être dangereux).
	- *Autoriser le repartage* : le destinataire peut le partager à un autre membre.
	- *Définir une date d'expiration* : le partage sera automatiquement rompu passée cette date.
	- *Note au destinataire* : précisez une courte information.
	- *Ne plus partager* : rompez le partage de ce fichier.

Le destinataire a ensuite accès à tous le contenu du dossier partagé, y compris les sous-dossiers.

![](./images/partage-dossier.gif)

#### Définissez les options de partage par défaut (admin)

Comme signalé dans la section [partager un dossier](partager-un-dossier), il peut être dangereux de laisser la possibilité à vos destinataires de supprimer des fichiers contenus dans un dossier partagé. Il serait bon d'éviter d'oublier, par mégarde, de laisser ce droit !

Si vous êtes administrateur, vous pouvez (et vous devriez) modifier les paramètres de partage par défaut. Pour cela :

* Ouvrez page des paramètres (cliquez sur l'icône de votre avatar ou initiale en haut à droite).
* À gauche, dans la section **Administration** ouvrez l'onglet **Partage**.
* Vous pouvez alors configurer les options de partage, dont désactiver l'autorisation de suppression par défaut. Chacun pourra toujours l'activer au moment de partager un dossier.

![](./images/20200831-221452.png)

#### Recevez et gérez un partage

Plaçons-nous du côté du destinataire. Les partages reçus sont automatiquement listés dans le dossier racine. Les icônes de dossier sont marquées par le symbole de partage. De même, l'icône de partage est remplacée par l'avatar de la personne qui a partagé avec vous.

Ici, l'utilisateur a reçu deux dossiers et un fichier.

![](./images/recevoir-partages.png)

Vous pouvez ensuite :

* déplacer ces partages dans vos propres dossiers ;
* les renommer.

Cela ne modifie en rien les fichiers et dossiers partagés chez leur propriétaire !

<!--
### Partagez hors de Nextcloud

### Commenter un document

## Agendas et contacts

## Travaillez en groupe

### Créer et administrer des groupes

### Rédiger à plusieurs

#### Prise de notes sur un pad

#### Outils de bureautique

### Discuter

### Gérer un projet

## Se fédérer avec un autre Nextcloud
-->
## Synchronisation

<!--
### Sur ordinateur
-->
### Sur smartphone

#### Application Nextcloud

##### Connexion

Cette application ne concerne que la partie dépôt de documents. Une fois l'application installée, celle-ci vous propose de vous connecter à un serveur Nextcloud.

![](./images/20200819-212209.png)

Allez ensuite sur votre Nextcloud via le Web, puis dans vos paramètres. Dans l'onglet *Sécurité*, tout en bas, vous pouvez *Créer un nouveau mot de passe d'application* (définissez éventuellement un nom tel que « Nextcloud mobile »).

![](./images/20200819-212833.png)

Vous pouvez simplement saisir l'URL de votre Nextcloud ainsi que vos nom d'utilisateur et mot de passe, ou, plus simplement, afficher un QR code qu'il vous reste à scanner avec votre mobile.

![](./images/20200819-213154.png)

Et scannez avec votre mobile.

![](./images/scan-qrcode.png)

<!--
##### Synchroniser automatiquement ses photos

#### Davx5

#### Nextcloud Deck
-->